package com.nav_chunchi

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nav_chunchi.adapter.HistoryAdapter
import com.nav_chunchi.database.DBHelper
import com.nav_chunchi.databinding.ActivityHistoryBinding
import com.nav_chunchi.model.HistoryItem


class HistoryActivity: AppCompatActivity() {

    private lateinit var binding: ActivityHistoryBinding
    var historyList: ArrayList<HistoryItem>? = null
    var historyAdapter: HistoryAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHistoryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val db = DBHelper(this, null)
        historyList = db.displayData()

        if(historyList!=null && !historyList!!.isEmpty()){
            historyAdapter = HistoryAdapter(historyList, this)
            val linearLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
            binding.rcvHistory.setLayoutManager(linearLayoutManager)
            binding.rcvHistory.setAdapter(historyAdapter)
        }


    }



}