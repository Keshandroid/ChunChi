package com.nav_chunchi.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.nav_chunchi.model.HistoryItem

class DBHelper(context: Context, factory: SQLiteDatabase.CursorFactory?) :
    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {

    // below is the method for creating a database by a sqlite query
    override fun onCreate(db: SQLiteDatabase) {
        // below is a sqlite query, where column names
        // along with their data types is given
        val query = ("CREATE TABLE " + TABLE_NAME + " ("
                + ID_COL + " INTEGER PRIMARY KEY, " +
                SPEED_COl + " TEXT," +
                AMOUNT_COL + " TEXT," +
                CURRENT_TIME + " TEXT" + ")")

        // we are calling sqlite
        // method for executing our query
        db.execSQL(query)
    }

    override fun onUpgrade(db: SQLiteDatabase, p1: Int, p2: Int) {
        // this method is to check if table already exists
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }

    // This method is for adding data in our database
    fun addName(speed: String, amount: String, currentTimeMillis: Long){

        // below we are creating
        // a content values variable
        val values = ContentValues()

        // we are inserting our values
        // in the form of key-value pair
        values.put(SPEED_COl, speed)
        values.put(AMOUNT_COL, amount)
        values.put(CURRENT_TIME,""+currentTimeMillis)

        // here we are creating a
        // writable variable of
        // our database as we want to
        // insert value in our database
        val db = this.writableDatabase

        // all values are inserted into database
        db.insert(TABLE_NAME, null, values)

        // at last we are
        // closing our database
        db.close()
    }

    // below method is to get
    // all data from our database
    fun getName(): Cursor? {

        // here we are creating a readable
        // variable of our database
        // as we want to read value from it
        val db = this.readableDatabase

        // below code returns a cursor to
        // read data from the database
        return db.rawQuery("SELECT * FROM " + TABLE_NAME, null)

    }

    fun displayData(): ArrayList<HistoryItem>? {
        val db = this.readableDatabase

        val cursor: Cursor = db.rawQuery("select *from speed_table", null)
        val modelArrayList: ArrayList<HistoryItem> = ArrayList()
        if (cursor.moveToFirst()) {
            do {
                modelArrayList.add(
                    HistoryItem(
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3)
                    )
                )
            } while (cursor.moveToNext())
        }
        cursor.close()
        return modelArrayList
    }

    companion object{
        // here we have defined variables for our database

        // below is variable for database name
        private val DATABASE_NAME = "CHUN_CHI"

        // below is the variable for database version
        private val DATABASE_VERSION = 1

        // below is the variable for table name
        val TABLE_NAME = "speed_table"

        // below is the variable for id column
        val ID_COL = "id"

        // below is the variable for name column
        val SPEED_COl = "speed"

        // below is the variable for age column
        val AMOUNT_COL = "amount"

        val CURRENT_TIME = "currentTime"
    }
}