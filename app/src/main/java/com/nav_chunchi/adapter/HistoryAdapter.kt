package com.nav_chunchi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.nav_chunchi.R
import com.nav_chunchi.model.HistoryItem
import java.text.SimpleDateFormat
import java.util.*


class HistoryAdapter(val modelArrayList: ArrayList<HistoryItem>?, context: Context) :
    RecyclerView.Adapter<HistoryAdapter.ViewHoder>() {
    private val context: Context

    //constructor
    init {
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHoder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_history, parent, false)
        return ViewHoder(view)
    }

    override fun onBindViewHolder(holder: ViewHoder, position: Int) {
        val model = modelArrayList!![position]
        holder.txtSpeed.setText(model.speed + "kmph")
        holder.txtCharge.setText(model.amount)
        holder.txtTime.setText(getDate(model.time.toLong(),"dd/MM/yyyy hh:mm:ss.SSS"))


    }

    override fun getItemCount(): Int {
        return modelArrayList!!.size
    }

    fun getDate(milliSeconds: Long, dateFormat: String?): String? {
        // Create a DateFormatter object for displaying date in specified format.
        val formatter = SimpleDateFormat(dateFormat)

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return formatter.format(calendar.time)
    }



    class ViewHoder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtSpeed: TextView
        val txtCharge: TextView
        val txtTime: TextView

        init {
            txtSpeed = itemView.findViewById(R.id.txtSpeed)
            txtCharge = itemView.findViewById(R.id.txtCharge)
            txtTime = itemView.findViewById(R.id.txtTime)
        }
    }
}